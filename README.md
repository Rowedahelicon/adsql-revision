## Please note: Regarding release 1.8.0 and below -> There are some pretty wacky security concerns in this, I will be uploading a new version soonish with everything stamped out. Until then, please continue with caution!

This is an update to the web interface of the AdsQl Advertisment plugin found on Alliedmodders.

https://forums.alliedmods.net/showthread.php?t=203966

It features a hastily yet function upgrade to Php 5.5.x standards. No more missing mysql errors! 
No other functionalities have been added though I am willing to add any reasonable requests should people have them.
I also may include my own edit of the plugin at some point.

This is provided as-is and may contain bugs! Please report any issues and I will attempt to solve them!

Thank you
--Rowdy

Version and credit history:

PHP 5.5.x compatibility upgrade
Project Forked by Rowedahelicon - June 2018
Version 1.8.0

Internet Explorer Compatibility & HTML Standards Compliance
by PharaohsPaw and PsychoBunny - March 2011
Version 1.7.7

Project Forked by PharaohsPaw - Feb 2011
Version 1.6.0

Extended for Server ID's and multi-game ads by PharaohsPaw 2011
Version 1.5_pp1.4.0

MySQL Advertisements System Originally Created/Designed by Gareth "StrontiumDog" Clarke ©2008
Version 1.5

Original Advertisements System by DJ Tsunami 2007
Login System with Admin Features by JPMaster77 ©2004
